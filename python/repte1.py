# Programa head_stdin.py
# Exercici 1. Mostrar les 5 primeres línies d'un fitxer. El nom del fitxer
# a mostrar es rep com a argument, sinó es rep, es mostren les cinc primeres línies
# de l'entrada estàndard.

import sys
# nom del fitxer
fitxer = sys.argv[1]

# si introduïm el número de línies
if len(sys.argv) > 2:
    numero = sys.argv[2]

    # obrim el fitxer
    with open(fitxer,'r') as file:
        linies = file.readlines()
        
        comptador = 0

        # per cada línia del fitxer imprimeix fins arribar al número que hem introduït
        for i in linies:
            comptador += 1
            if comptador <= int(numero):
                print(i.strip())

# sinó, mostra les 5 primeres línies
else:
    with open(fitxer,'r') as file:
        linies = file.readlines()
        
        comptador = 0
        for i in linies:
            comptador += 1
            if comptador <= 5:
                print(i.strip())