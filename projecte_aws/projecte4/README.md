## Crear una imatge AMI personalitzada
### Crear una AMI basada en el servidor web apache
**Servidor web**
```


```

<img src="imatges/imatge1.png">

#### Crear una AMI basada en la màquina que incorpora docker i els serveis web i net
```
[ec2-user@ip-172-31-36-55 ~]$ sudo passwd
Changing password for user root.
New password: 
BAD PASSWORD: The password is shorter than 8 characters
Retype new password: 
passwd: all authentication tokens updated successfully.
```

```
[ec2-user@ip-172-31-36-55 ~]$ sudo passwd
Changing password for user root.
New password: 
BAD PASSWORD: The password is shorter than 8 characters
Retype new password: 
passwd: all authentication tokens updated successfully.
```

```
sudo yum install git nmap tree
```

```
[ec2-user@ip-172-31-36-55 ~]$ git clone https://www.gitlab.com/edtasixm05/aws
Cloning into 'aws'...
warning: redirecting to https://gitlab.com/edtasixm05/aws.git/
remote: Enumerating objects: 83, done.
remote: Counting objects: 100% (83/83), done.
remote: Compressing objects: 100% (82/82), done.
remote: Total 83 (delta 43), reused 0 (delta 0), pack-reused 0
Receiving objects: 100% (83/83), 21.29 MiB | 12.15 MiB/s, done.
Resolving deltas: 100% (43/43), done.
[ec2-user@ip-172-31-36-55 ~]$ git clone https://www.gitlab.com/edtasixm05/docker
Cloning into 'docker'...
warning: redirecting to https://gitlab.com/edtasixm05/docker.git/
remote: Enumerating objects: 1045, done.
remote: Counting objects: 100% (427/427), done.
remote: Compressing objects: 100% (224/224), done.
remote: Total 1045 (delta 182), reused 427 (delta 182), pack-reused 618
Receiving objects: 100% (1045/1045), 9.07 MiB | 39.85 MiB/s, done.
Resolving deltas: 100% (479/479), done.
```

```
[ec2-user@ip-172-31-36-55 ~]$ sudo curl -sl https://github.com/docker/compose/releases/download/v2.6.0/docker-compose-linux-x86_64 -o /usr/local/bin/docker-compose

[ec2-user@ip-172-31-36-55 ~]$ sudo chmod +x /usr/local/bin/docker-compose # que sigui executable
[ec2-user@ip-172-31-36-55 ~]$ sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose # enllaç simbòlic

[ec2-user@ip-172-31-36-55 ~]$ docker-compose version
Docker Compose version v2.6.0
```



### Comprovacions
Creem una instància amb la imatge que ens hem fabricat


#### Creem la instància

Creem la instància i li posem un nom

<img src="imatges/name.png">



Seleccionem la imatge que hem creat

<img src="imatges/ami.png">



El tipus d'instància utilitzem la t2

<img src="imatges/instancia.png">



Seleccionem la clau que hem creat

<img src="imatges/keypair.png">



A network settings, seleccionem el security group

<img src="imatges/network.png">


#### comprovacions terminal
```
[ec2-user@ip-172-31-16-55 ~]$ docker ps
CONTAINER ID   IMAGE                 COMMAND                  CREATED        STATUS                  PORTS                                                                                                                            NAMES
8ccf6394facb   sarayj03/repte7_net   "/bin/sh -c /opt/doc…"   29 hours ago   Up Less than a second   0.0.0.0:7->7/tcp, :::7->7/tcp, 0.0.0.0:13->13/tcp, :::13->13/tcp, 20-21/tcp, 69/tcp, 0.0.0.0:19->19/tcp, :::19->19/tcp, 80/tcp   net.edt.org
```


```
[ec2-user@ip-172-31-16-55 ~]$ nmap localhost
Starting Nmap 7.93 ( https://nmap.org ) at 2023-12-05 17:18 UTC
Nmap scan report for localhost (127.0.0.1)
Host is up (0.00030s latency).
Not shown: 995 closed tcp ports (conn-refused)
PORT   STATE SERVICE
7/tcp  open  echo
13/tcp open  daytime
19/tcp open  chargen
22/tcp open  ssh
80/tcp open  http

Nmap done: 1 IP address (1 host up) scanned in 0.11 seconds
```


```
[ec2-user@ip-172-31-16-55 ~]$ docker-compose version
Docker Compose version v2.6.0
```


```
[ec2-user@ip-172-31-16-55 ~]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
     Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; preset: disabled)
     Active: active (running) since Tue 2023-12-05 17:14:30 UTC; 4min 30s ago
       Docs: man:httpd.service(8)
   Main PID: 1982 (httpd)
     Status: "Total requests: 0; Idle/Busy workers 100/0;Requests/sec: 0; Bytes served/sec:   0 B/sec"
      Tasks: 177 (limit: 1114)
     Memory: 17.8M
        CPU: 212ms
     CGroup: /system.slice/httpd.service
             ├─1982 /usr/sbin/httpd -DFOREGROUND
             ├─2013 /usr/sbin/httpd -DFOREGROUND
             ├─2014 /usr/sbin/httpd -DFOREGROUND
             ├─2015 /usr/sbin/httpd -DFOREGROUND
             └─2016 /usr/sbin/httpd -DFOREGROUND

Dec 05 17:14:29 ip-172-31-36-55.ec2.internal systemd[1]: Starting httpd.service - The Apache HTTP Server>
Dec 05 17:14:30 ip-172-31-36-55.ec2.internal httpd[1982]: Server configured, listening on: port 80
Dec 05 17:14:30 ip-172-31-36-55.ec2.internal systemd[1]: Started httpd.service - The Apache HTTP Server.
```


**Comprovacions servidor net**
+ echo
```
[ec2-user@ip-172-31-16-55 ~]$ telnet localhost 7
Trying 127.0.0.1...
Connected to localhost.
Escape character is '^]'.
hola
hola
^]
telnet> quit
Connection closed.
```

+ date
```
[ec2-user@ip-172-31-16-55 ~]$ telnet localhost 13
Trying 127.0.0.1...
Connected to localhost.
Escape character is '^]'.
05 DEC 2023 17:20:29 UTC
Connection closed by foreign host.
```

+ chargen
```
[ec2-user@ip-172-31-16-55 ~]$ telnet localhost 19
...
...
...
/0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuv
0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvw
123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwx
23456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxy
3456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz
456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{
56789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|
6789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}
789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}!
89:;<=>?@ABCDEFGHIJKLMNOPQRST
telnet> quit
```


#### Comprovació pàgina web

<img src="imatges/pagina.png">














