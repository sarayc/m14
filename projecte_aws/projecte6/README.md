## Crear una màquina virtual Windows
Crearem una màquina virtual windows, i podrem accedir via:
+ consola a través de SSH
+ a través d'un client d'escriptori remot com per exemple Remmina

### 6.1. Accedir amb un client d'escriptori remot
Generar una màquina virtual windows i accedir-hi amb un client d'escriptori remot
Creem la instància Windows, en seleccionar Windows informa que canviarà el Security Groups això és perquè ara l'accés ha de permetre RDP remote Desktop protocol, per permetre accedir a través d'un client d'escriptori remot.


Posem nom i seleccionem que volem un windows


<img src="imatges/imatge1.png">


Deixen per defecte la instància que tenim i seleccionem la nostra keypair


<img src="imatges/imatge2.png">


Creem una nova network settings


<img src="imatges/imatge3.png">


Observem que ara a configure storge tenim 30GiB perquè com estem en un windows si posem 8 ens donaria un error


<img src="imatges/imatge4.png">


Ja la hem creat, anem a security i observem que ara tenim el port 3389 obert que és per connectar-se per RDP.


<img src="imatges/imatge5.png">



Ara anem on posa "connect" i a l'apartat de "RDP client" que és el que utilitzarem, i important descarregar el fitxer.


<img src="imatges/imatge6.png">


si obrim aquest fitxer amb el geany podem observar que ens surt la configuració per connectar-se per RDP


<img src="imatges/imatge7.png">


Si volem entrar necessitem necessitem la clau privada, per tant li donem a "Get password" 
Ens surt aquesta finestra on hem de pujar la clau privada que tenim nosaltres.


<img src="imatges/imatge8.png">


Quan ja hem pujat la clau privada ara ja ens dona la contrasenya. El password es genera automàticament utilitzant la clau que hem generat nosaltres. Si la clau privada és correcte ens donarà la contrasenya.


<img src="imatges/imatge9.png">

Per poder entrar cliquem sobre el fitxer que ens hem descarregat i ens obrirà el remmina.


<img src="imatges/imatge10.png">

<img src="imatges/imatge11.png">






### 6.2. Accedir amb client SSH
Generar una màquina virtual Windows i accedir-hi amb un client SSH i/o amb Putty

### 6.3. Crear una màquina virtual windows que ofereix algun tipus de servei de xarxa

### 6.4. Generar un domini AD windows amb un client del domini
