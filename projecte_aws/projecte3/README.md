## Generar un servidor web amb apache
#### 3.1. Crear una VM amb un servei web apache2 i configurar els ports
**Launch instance**
+ name: myweb
+ amazon
+ instància: t2
+ keypair: hem agafat la que tenim per defecte
+ network settings
+ configure storage

**per entrar**
```
mv clau .ssh/.
chmod 600 .ssh/clau
ssh -i .ssh/clau ec2-user@ip
```

**instal·lar el servidor**
```
sudo yum update
sudo dnf install httpd

sudo systemctl start http
```

Per comprovar que tenim el servidor des de la terminal
```
wget localhost
```


#### 3.2. Gestionar security groups
```
Anem a security i afegim el port 80
```


#### 3.3. Afegir docker a la màquina virtual i desplegar un servei
**Afegim  docker**
Idenfitiquem el sistema operatiu
```
[ec2-user@ip-172-31-36-55 ~]$ uname -a
Linux ip-172-31-36-55.ec2.internal 6.1.61-85.141.amzn2023.x86_64 #1 SMP PREEMPT_DYNAMIC Wed Nov  8 00:39:18 UTC 2023 x86_64 x86_64 x86_64 GNU/Linux


[ec2-user@ip-172-31-36-55 ~]$ cat /etc/os-release
NAME="Amazon Linux"
VERSION="2023"
ID="amzn"
ID_LIKE="fedora"
VERSION_ID="2023"
PLATFORM_ID="platform:al2023"
PRETTY_NAME="Amazon Linux 2023"
ANSI_COLOR="0;33"
CPE_NAME="cpe:2.3:o:amazon:amazon_linux:2023"
HOME_URL="https://aws.amazon.com/linux/"
BUG_REPORT_URL="https://github.com/amazonlinux/amazon-linux-2023"
SUPPORT_END="2028-03-15"
```

Identifiquem si porta docker incoporat o no
```
[ec2-user@ip-172-31-36-55 ~]$ docker ps
-bash: docker: command not found

# llista tots els repositoris actius
[ec2-user@ip-172-31-36-55 ~]$ yum repolist 
repo id                                                 repo name
amazonlinux                                             Amazon Linux 2023 repository
kernel-livepatch                                        Amazon Linux 2023 Kernel Livepatch repository
```

Instal·lem docker
```
sudo yum install docker
sudo systemctl status docker 
sudo usermod -aG docker ec2-user # ens afegim al grup docker, important fem un restart sinó no estarem al grup docker
```


**comprovació tenim docker**
```
[ec2-user@ip-172-31-36-55 ~]$ docker ps
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
```


**despleguem un servei**
```
[ec2-user@ip-172-31-36-55 ~]$ docker network create mynet
46790eeda5be051a4a3d854814baf2e3ee44b80c4644ecdc8dffd8e4b6947376

[ec2-user@ip-172-31-36-55 ~]$ docker run --rm --name net.edt.org -h net.edt.org --net mynet -p 7:7 -p 13:13 -p 19:19 -d sarayj03/repte7_net
```

Instal·lem nmap per poder veure els ports que tenim oberts i també installem telnet
```
sudo yum install nmap
[ec2-user@ip-172-31-36-55 ~]$ nmap localhost
Starting Nmap 7.93 ( https://nmap.org ) at 2023-12-04 10:52 UTC
Nmap scan report for localhost (127.0.0.1)
Host is up (0.00028s latency).
Not shown: 996 closed tcp ports (conn-refused)
PORT   STATE SERVICE
7/tcp  open  echo
13/tcp open  daytime
19/tcp open  chargen
22/tcp open  ssh

sudo yum install telnet
```

Mirem la ip que tenim al container
```
[ec2-user@ip-172-31-36-55 ~]$ docker network inspect mynet | grep IP
        "EnableIPv6": false,
        "IPAM": {
                "IPv4Address": "172.18.0.2/16",
                "IPv6Address": ""
```

Ports que tenim oberts al container
```
[ec2-user@ip-172-31-36-55 ~]$ nmap 172.18.0.2
Starting Nmap 7.93 ( https://nmap.org ) at 2023-12-04 10:54 UTC
Nmap scan report for ip-172-18-0-2.ec2.internal (172.18.0.2)
Host is up (0.00021s latency).
Not shown: 995 closed tcp ports (conn-refused)
PORT   STATE SERVICE
7/tcp  open  echo
13/tcp open  daytime
19/tcp open  chargen
21/tcp open  ftp
80/tcp open  http

Nmap done: 1 IP address (1 host up) scanned in 0.08 seconds
```

Si verifiquem des d'un client de l'aula
```
[ec2-user@ip-172-31-36-55 ~]$ telnet 54.208.214.29 13
Trying 54.208.214.29...
^C  
```
No ens funciona perquè no tenim obert els security groups.
Anem a Security groups i modifiquem:
<img src="ports.png">


**comprovació dels ports**
+ echo
```
[ec2-user@ip-172-31-36-55 ~]$ telnet 54.208.214.29 7
Trying 54.208.214.29...
Connected to 54.208.214.29.
Escape character is '^]'.
servidor echo
servidor echo
^]
telnet> quit
Connection closed.
```

+ date
```
[ec2-user@ip-172-31-36-55 ~]$ telnet 54.208.214.29 13
Trying 54.208.214.29...
Connected to 54.208.214.29.
Escape character is '^]'.
04 DEC 2023 11:04:41 UTC
Connection closed by foreign host.
```

+ chargen
```
[ec2-user@ip-172-31-36-55 ~]$ telnet 54.208.214.29 19 | head -n10
Trying 54.208.214.29...
Connected to 54.208.214.29.
Escape character is '^]'.
!"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefgh
"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghi
#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghij
$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijk
%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijkl
&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklm
'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmn
```
