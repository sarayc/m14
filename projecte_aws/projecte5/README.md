## Assignar una adreça IP flotant a una màquina virtual

### Assignar una adreça IP flotant
+ Crear una adreça IP: Allocate Elastic IP address
+ associar l'adreça IP a una instància: Associate IP address
+ accions
	+ view details
	+ release elastic IP address
	+ associate elastic IP address
	+ disassociate elastic IP address
	+ update reverse DNS


#### Crear una adreça IP
Quan anem a una instància a "Network and security" tenim un apartat de Elastic IPs on podem crear una elastic IP addresss

<img src="imatges/ip.png">


#### Associar l'adreça IP a una instància
Ara associem la ip address a una instància amb "associate IP adress"
<img src="imatges/associar.png">



Verifiquem la nova adreça IP
```
saray@debian:~$ ssh -i .ssh/vockey.pem ec2-user@34.229.98.230
The authenticity of host '34.229.98.230 (34.229.98.230)' can't be established.
ECDSA key fingerprint is SHA256:HmtHTQjZTyZSEWG5nQuHL3TFtfQJg86LWkbpLqSpgJg.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '34.229.98.230' (ECDSA) to the list of known hosts.
   ,     #_
   ~\_  ####_        Amazon Linux 2023
  ~~  \_#####\
  ~~     \###|
  ~~       \#/ ___   https://aws.amazon.com/linux/amazon-linux-2023
   ~~       V~' '->
    ~~~         /
      ~~._.   _/
         _/ _/
       _/m/'
Last login: Mon Dec  4 12:42:57 2023 from 79.154.160.199
[ec2-user@ip-172-31-36-55 ~]$ 
[ec2-user@ip-172-31-36-55 ~]$ 
```


```
saray@debian:~/m14/projecte_aws/projecte5$ nmap 34.229.98.230
Starting Nmap 7.80 ( https://nmap.org ) at 2023-12-10 11:56 CET
Nmap scan report for ec2-34-229-98-230.compute-1.amazonaws.com (34.229.98.230)
Host is up (0.097s latency).
Not shown: 995 filtered ports
PORT   STATE  SERVICE
7/tcp  closed echo
13/tcp closed daytime
19/tcp closed chargen
22/tcp open   ssh
80/tcp open   http

Nmap done: 1 IP address (1 host up) scanned in 7.75 seconds
```


Pàgina web

<img src="imatges/pagina.png">




### Associar l'adreça IP flotant a una altra instància
+ Dissociar l'adreça IP flotant de la instància que l'està utilitzant actualment
+ alliberar (release) l'adreça IP flotant.


Dissociar l'adreça IP flotant de la instància

<img src="imatges/dissociate.png">


Fem l'associació ip address

<img src="imatges/img4.png">
