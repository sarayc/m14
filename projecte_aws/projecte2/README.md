## Crear la primera VM Linux a AWS EC2
### Objectius
+ crear una màquina virtual GNU/Linux i accedir-hi
+ accedir via SSH a una instància GNU/Linux
+ crear, engegar, aturar i destruir màquines virtuals
+ generar templates per fer màquines virtuals de característiques similars

#### Crear una màquina virtual GNU/Linux
1.Accedim a EC2 Dashboard
2.Seleccionem la regió London
3.Fem launch instance
    + name
    + seleccionem el sistema operatiu
    + la instance type que volem
    + key pair: generem un par de keypair
    + network settings: ho deixem per defecte com està
    + configure storage: ho deixem per defecte com està

#### Accedir via SSH a la instància
1.La ip que posarem al ssh serà la adreça IP pública de la instància
2.Assignem permissos 400 al fitxer de les claus que hem creat
```
ssh -i .ssh/keypair_practica2 ec2-user@ip
```

#### Crear, engegar, aturar i destruir màquines virtuals
+ engegar: start
+ aturar: stop
+ eliminar: terminate


#### Generar templates
Crearem una instància usant un Template. El template no copia els continguts de la imatge, simplement descriu, recull, les opcions que hem seleccionat en el procés de Lauch. 
