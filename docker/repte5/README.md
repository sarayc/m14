### ldapserver 2023
**Fitxers**
+ Dockerfile
+ startup.sh
+ edt-org.lif
+ slapd.conf

Dockerfile
```
# ldapserver
FROM debian:latest
LABEL subject="m14 2023"
RUN apt-get update
RUN apt-get install -y procps iproute2 vim tree nmap ldap-utils slapd less
ARG DEBIAN_FRONTED=noninteractive

RUN mkdir /opt/docker
COPY * /opt/docker
RUN chmod +x /opt/docker/startup.sh 
WORKDIR /opt/docker
ENTRYPOINT  ["/opt/docker/startup.sh"]
EXPOSE 389
```

startup.sh
```
#! /bin/bash

function initdb(){
	# Esborrar els directoris de configuració i de dades.
	rm -rf /etc/ldap/slapd.d/*
	rm -rf /var/lib/ldap/*

	# Generar el directori de configuració dinàmica slapd.d a partir del fitxer de configuració slapd.conf
	slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d

	# Injectar a baix nivell les dades de la BD 'populate' de l'organització dc=edt,dc=org
	slapadd -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif

	# Assignar la propietat i grup del directori de dades i de configuració a l'usuari openldap.
	chown -R openldap:openldap /etc/ldap/slapd.d /var/lib/ldap

	# Engegar el servei slapd amb el paràmetre que fa debug per mantenir-lo engegat en foreground.
	/usr/sbin/slapd -d0

}


function slapd(){
	# Esborrar els directoris de configuració i de dades.
	rm -rf /etc/ldap/slapd.d/*
	rm -rf /var/lib/ldap/*

	# Generar el directori de configuració dinàmica slapd.d a partir del fitxer de configuració slapd.conf
	slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d

	# Assignar la propietat i grup del directori de dades i de configuració a l'usuari openldap.
	chown -R openldap:openldap /etc/ldap/slapd.d /var/lib/ldap

	# Engegar el servei slapd amb el paràmetre que fa debug per mantenir-lo engegat en foreground.
	/usr/sbin/slapd -d0
}


function start(){
	/usr/sbin/slapd -d0 
}


case $1 in
  initdb)
    echo "initdb"
    initdb;;
  slapd)
    slapd;;
  start)
    echo "start"
    start;;
  slapcat)
    if [ $2 -eq 0 -o $2 -eq 1 ] 
    then
      slapcat -n$2
    else
      slapcat
    fi;;
  *)
    start;;
esac
```

#### Ordres
**Generem la imatge**
```
docker build -t sarayj03/repte5 .
```

#### Engegem amb initdb
**container: ho inicialitzat tot de nou i fa el populate de edt.org**
```
docker run --rm --name repte5 -h ldap.edt.org -d sarayj03/repte5 initdb
```

Provem
```
ldapsearch -x -LLL -H ldap://172.17.0.2 -b 'dc=edt,dc=org'
```


#### Engegem amb slapd
**container: ho inicialitza tot però només engega el servidor, sense posar-hi dades**
```
docker run --rm --name repte5 -h ldap.edt.org -d sarayj03/repte5 slapd
```

Provem 
```
ldapsearch -x -LLL ldap://172.17.0.2 -b 'dc=edt,dc=org'
```
No tenim cap dada


#### Engegem amb start
**container: engegar el servidor utilitzant la persistència de dades de la db i de la configuració.**
+ Creem els volums
```
docker volume create ldap-dades
docker volume create ldap-config
```

+ engegem ja amb persistència de dades
```
docker run --rm --name repte5 -h ldap.edt.org -v ldap-dades:/var/lib/ldap -v ldap-config:/etc/ldap/slapd.d -d sarayj03/repte5 start
```

+ fem un ldapsearch 
```
docker exec -it repte5 ldapsearch -x -LLL -H ldap://172.17.0.2 -b 'dc=edt,dc=org'
```

+ Eliminem a l'usuari Jordi per exemple
```
ldapdelete -x -H ldap://172.17.0.2 -D 'cn=Manager,dc=edt,dc=org' -w secret "cn=Jordi Mas,ou=usuaris,dc=edt,dc=org"
ldapsearch -x -LLL -H ldap://172.17.0.2 -b 'dc=edt,dc=org' uid=Jordi
```

+ parem el container i comprovem que l'usuari Jordi no hi es:
```
docker stop repte5
```

#### Comprovacions slapcat
**Slapcat**
```
docker run --rm --name repte5 -h ldap.edt.orgt -it sarayj03/repte5 slapcat 0
```
