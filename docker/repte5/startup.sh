#! /bin/bash

function initdb(){
	# Esborrar els directoris de configuració i de dades.
	rm -rf /etc/ldap/slapd.d/*
	rm -rf /var/lib/ldap/*

	# Generar el directori de configuració dinàmica slapd.d a partir del fitxer de configuració slapd.conf
	slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d

	# Injectar a baix nivell les dades de la BD 'populate' de l'organització dc=edt,dc=org
	slapadd -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif

	# Assignar la propietat i grup del directori de dades i de configuració a l'usuari openldap.
	chown -R openldap:openldap /etc/ldap/slapd.d /var/lib/ldap

	# Engegar el servei slapd amb el paràmetre que fa debug per mantenir-lo engegat en foreground.
	/usr/sbin/slapd -d0

}


function slapd(){
	# Esborrar els directoris de configuració i de dades.
	rm -rf /etc/ldap/slapd.d/*
	rm -rf /var/lib/ldap/*

	# Generar el directori de configuració dinàmica slapd.d a partir del fitxer de configuració slapd.conf
	slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d

	# Assignar la propietat i grup del directori de dades i de configuració a l'usuari openldap.
	chown -R openldap:openldap /etc/ldap/slapd.d /var/lib/ldap

	# Engegar el servei slapd amb el paràmetre que fa debug per mantenir-lo engegat en foreground.
	/usr/sbin/slapd -d0
}


function start(){
	/usr/sbin/slapd -d0 
}


case $1 in
  initdb)
    echo "initdb"
    initdb;;
  slapd)
    slapd;;
  start)
    echo "start"
    start;;
  slapcat)
    if [ $2 -eq 0 -o $2 -eq 1 ] 
    then
      slapcat -n$2
    else
      slapcat
    fi;;
  *)
    start;;
esac

