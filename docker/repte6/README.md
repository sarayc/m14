### variables d'entorn
**Fitxers**
+ dockerfile
+ startup.sh
+ edt-org.ldif
+ slapd.conf

#### Primera manera de fer-ho
#### Fitxer Dockerfile
```
# Ldapserver 2023
FROM debian:latest
LABEL version="1.0"
LABEL author="@sarayc AISX-M06"
LABEL subject="ldalserver 2023"
ENV admin=saray
ENV password=jupiter
RUN apt-get update
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get install -y procps iproute2 tree nmap vim ldap-utils slapd less
RUN mkdir /opt/docker
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
EXPOSE 389
```

Canviem el dockerhub posem una variable d'entorn
**Generem la imatge**
```
docker build -t sarayj03/repte6 .
```

**Engegem la imatge**
```
docker run --rm --name repte6 -h ldap.edt.org -d sarayj03/repte6
```

**Entrem al container**
```
docker exec -it repte6 /bin/bash
```

Si fem un cat slapd.conf podem veure com ara l'adminsitrador és saray


#### Fitxer Dockerfile
```
# Ldapserver 2023
FROM debian:latest
LABEL version="1.0"
LABEL author="@sarayc AISX-M06"
LABEL subject="ldalserver 2023"
RUN apt-get update
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get install -y procps iproute2 tree nmap vim ldap-utils slapd less
RUN mkdir /opt/docker
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
EXPOSE 389
```

#### Segona manera de fer-ho
**Generem la imatge**
```
docker build -t sarayj03/repte6 .
```

Al Dockerfile no posem env perquè li passarem amb -e
**Engegem la imatge**
```
docker run --rm --name repte6 -h ldap.edt.org -e admin=saray -e password=jupiter -d sarayj03/repte6
```

**Entrem al container**
```
docker exec -it repte6 /bin/bash
```
