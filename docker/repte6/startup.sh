#! /bin/bash
echo "Configurant el servidor ldap..."


# Esborrar els directoris de configuració i de dades
rm -rf /var/lib/ldap/*
rm -rf /etc/ldap/slapd.d/*

# Generar el directori de configuració dinàmica slapd.d a partir del fitxer de configuració slapd.conf
sed -i 's/Manager/'$admin'/g' /opt/docker/slapd.conf
#sed -r -i '/rootdn/ s/(.*)/rootdn "'$admin'"' /opt/docker/slapd.conf
sed -i 's/secret/'$password'/g' /op/docker/slapd.conf
slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d

# Injectar a baix nivell les dades de la BD 'populate' de l'organització dc=edt,dc=org
slapadd -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif

# Assignar la propietat i grup del directori de ddaes i de configuració a l'usuari openldap
chown -R openldap:openldap /etc/ldap/slapd.d /var/lib/ldap

# Engegar el servei slapd amb el paràmetre que fa debug per mantenir-lo engegat en foreground
/usr/sbin/slapd -d0
