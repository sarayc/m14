# servidor net: echo, daytime, chargen, ftp, tftp, http
## Fitxers
+ Dockerfile
+ startup.sh
+ chargen
+ echo
+ daytime
+ index.html
+ useradd.sh
+ vsftpd.conf


### Fitxer Dockerfile
```
# servidor net
FROM debian:latest
LABEL author="@sarayc M05"
RUN apt-get update
RUN apt-get -y install iproute2 procps nmap tree vim xinetd net-tools apache2 iputils-ping tftpd-hpa vsftpd telnet wget ftp tftp
RUN mkdir /opt/docker
COPY daytime echo chargen /etc/xinetd.d/
COPY index.html /var/www/html/
COPY vsftpd.conf /etc/vsftpd.conf
COPY useradd.sh /opt/docker/useradd.sh
RUN bash /opt/docker/useradd.sh
RUN chmod 775 /srv/ftp
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
EXPOSE 7 13 19 80 20 21 69 
```

+ explicació Dockerfile ftp: afegim un fitxer "useradd.sh" per poder tenir usuaris quan entrem
+ canviem els permisos del directori /srv/ftp perquè formarem part del grup ftp i si no els canviem no tindrem permis per poder crear...

#### Ports:
+ 7 --> echo
+ 13 --> date
+ 19 --> chargen
+ 80 --> http
+ 21 --> ftp: utilitzat per connectar-se de forma remota a un servidor i autentificar-se 
+ 20 --> ftp: transferència d'arxius una vegada autentificat
+ 69 --> tftp


### Fitxer startup.sh
```
#! /bin/bash
service vsftpd start 
apachectl start
#apachectl -k start -X 
/usr/sbin/xinetd -dontfork
```


### Fitxer useradd.sh
```
#! /bin/bash
# Creació d'usuaris prefixats per verificar accés ssh
for user in unix1 unix2 unix3
do
	useradd -m $user
	echo $user:$user | chpasswd
	usermod -aG ftp $user
done
```
+ explicació: creem usuaris, li assignem contrasenya i finalment ho posem dins del grup ftp


### Fitxer vsftpd.conf
```
anonymous_enable=YES
local_enable=YES
write_enable=YES
local_root=/srv/ftp
```
+ explicació: descomentem i afegim la línia "local_root=/srv/ftp" per crear aquest directori 


### Copiar els fitxers
Entrem al container
```
docker cp net:/etc/xinetd/daytime .
docker cp net:/etc/xinetd/echo .
docker cp net:/etc/xinetd/chargen .
```


## Ordres per executar
**Generem la imatge**
```
docker build -t sarayj03/repte7_net .
```


**Engegem el container**
```
docker run --rm --name net -h net.edt.org --net 2hisx -d sarayj03/repte7_net
```


**Engegem un altre container**
```
docker run --rm --name client --net 2hisx -it sarayj03/repte7_net /bin/bash
```


**ip de la net 2hisx**
```
docker inspect 2hisx | grep IP
       "EnableIPv6": false,
        "IPAM": {
                "IPv4Address": "172.18.0.3/16",
                "IPv6Address": ""
                "IPv4Address": "172.18.0.2/16",
                "IPv6Address": ""
```



## Comprovacions:
Recordem que entrem en un container per poder provar els servidors
```
docker run --rm --name client --net 2hisx -it sarayj03/repte7_net /bin/bash
```

### servidor xinetd

**echo**
```
root@43cd9856385e:/opt/docker# telnet 172.18.0.2 7
Trying 172.18.0.2...
Connected to 172.18.0.2.
Escape character is '^]'.
hola
hola
^]
```


**date**
```
root@43cd9856385e:/opt/docker# telnet 172.18.0.2 13
Trying 172.18.0.2...
Connected to 172.18.0.2.
Escape character is '^]'.
24 OCT 2023 17:57:57 UTC
Connection closed by foreign host.
```


**chargen**
```
telnet 172.18.0.2 19
9:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}!"#
:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}!"#$
;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}!"#$%
<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}!"#$%&
=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}!"#$%&'
>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}!"#$%&'(
?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}!"#$%&'()
@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}!"#$%&'()*
ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}!"#$%&'()*+
BCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}!"#$%&'()*+,
CDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}!"#$%&'()*+,-
DEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}!"#$%&'()*+,-.
EFGHIJKLMNOPQRSTUVWXYZ[\]^_`a
```


### servidor hhtp
Al navegador
```
localhost
172.18.0.2
```

**Comprovació wget**
Entrem al container interactiu i anem a /tmp per exemple
```
root@255f1c652cd0:/tmp# ls
root@255f1c652cd0:/tmp# wget 172.18.0.2
--2023-10-24 17:49:38--  http://172.18.0.2/
Connecting to 172.18.0.2:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 15 [text/html]
Saving to: 'index.html'

index.html                 100%[=====================================>]      15  --.-KB/s    in 0s      

2023-10-24 17:49:38 (2.21 MB/s) - 'index.html' saved [15/15]
```


### servidor ftp
**Entrem com a anonymous**
```
root@a97931534a40:/opt/docker# ftp 172.18.0.2
Connected to 172.18.0.2.
220 (vsFTPd 3.0.3)
Name (172.18.0.2:root): anonymous
331 Please specify the password.
Password: 
230 Login successful.
Remote system type is UNIX.
Using binary mode to transfer files.
ftp> 
```


**Comprovació**
```
root@net:/opt/docker# ftp localhost 
Trying 127.0.0.1:21 ...
Connected to localhost.
220 (vsFTPd 3.0.3)
Name (localhost:root): saray
331 Please specify the password.
Password: 
230 Login successful.
Remote system type is UNIX.
Using binary mode to transfer files.
ftp> mkdir programacio
257 "/srv/ftp/programacio" created
```

**comprovació en un altre container**
```
root@cfdadbfe3a4d:/opt/docker# ftp 172.18.0.2
Connected to 172.18.0.2.
220 (vsFTPd 3.0.3)
Name (172.18.0.2:root): saray
331 Please specify the password.
Password: 
230 Login successful.
Remote system type is UNIX.
Using binary mode to transfer files.
ftp> ls
```

**De manera local**
```
root@net:/opt/docker# ftp localhost
Trying 127.0.0.1:21 ...
Connected to localhost.
220 (vsFTPd 3.0.3)
Name (localhost:root): unix1
331 Please specify the password.
Password: 
230 Login successful.
Remote system type is UNIX.
Using binary mode to transfer files.
ftp> 
```


**en un altre container**
```
root@7366a30ed759:/opt/docker# ftp 172.18.0.2
Connected to 172.18.0.2.
220 (vsFTPd 3.0.3)
Name (172.18.0.2:root): unix1
331 Please specify the password.
Password: 
230 Login successful.
Remote system type is UNIX.
Using binary mode to transfer files.
ftp> 
```

### Servidor tftp

```
ls -l /srv/tftp
```




















