#! /bin/bash
# Creació d'usuaris prefixats per verificar accés ssh
for user in unix1 unix2 unix3
do
	useradd -m $user
	echo $user:$user | chpasswd
done
