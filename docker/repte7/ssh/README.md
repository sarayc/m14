### servidor ssh
#### Fitxers
+ Dockerfile
+ useradd.sh

#### Fitxer dockerfile
```
# servidor ssh 
FROM debian:latest
LABEL author="@sarayc"
RUN apt-get update
RUN apt-get install -y openssh-client openssh-server
RUN mkdir /opt/docker
COPY * /opt/docker
COPY useradd.sh /opt/docker
RUN chmod +x /opt/docker/useradd.sh
RUN bash /opt/docker/useradd.sh
WORKDIR /opt/docker
CMD /etc/init.d/ssh start -D
EXPOSE 22
```

#### Fitxer startup.sh
```
#! /bin/bash
# Creació d'usuaris prefixats per verificar accés ssh
for user in unix1 unix2 unix3
do
	useradd -m $user
	echo $user:$user | chpasswd
done
```

#### Ordres 
**Generem la imatge**
```
docker build -t sarayj03/repte7_ssh .
```

**Engegem el servidor ssh**
```
docker run --rm --name ssh -h ssh.edt.org --net 2hisx -d sarayj03/repte7_ssh
```


#### Comprovacions
**localment**
```
docker exec -it ssh /bin/bash
```
```
root@ssh:/opt/docker# ssh unix1@localhost
unix1@localhost's password: 
Linux ssh.edt.org 6.1.0-9-amd64 #1 SMP PREEMPT_DYNAMIC Debian 6.1.27-1 (2023-05-08) x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
Last login: Mon Oct 23 11:06:07 2023 from 172.17.0.3
$ 
```

**com a client**
```
docker run --rm --name client_ssh --net 2hisx -it sarayj03/repte7_ssh /bin/bash
```
```
root@9c8a74963ae3:/opt/docker# ssh unix2@172.18.0.2
unix2@172.18.0.2's password: 
Linux ssh.edt.org 6.1.0-9-amd64 #1 SMP PREEMPT_DYNAMIC Debian 6.1.27-1 (2023-05-08) x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
$ 
```
