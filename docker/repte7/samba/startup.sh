#! /bin/bash
# creació de carpetes
mkdir /var/lib/samba/public
chmod 777 /var/lib/samba/public
cp /opt/docker/* /var/lib/samba/public/.

#mkdir /var/lib/samba/privat
#chmod 777 /var/lib/samba/privat

# Configuració de samba
cp /opt/docker/smb.conf /etc/samba/smb.conf

# configuració usuaris
useradd -m -s /bin/bash lila

# Activar els serveis
/usr/sbin/smbd && echo "smb Ok"
/usr/sbin/nmbd -F && echo "nmb Ok"
