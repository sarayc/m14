### servidor samba interactivament
```
docker run --rm --name samba -it debian /bin/bash
```
```
apt-get update
apt-get install samba vim smbclient 
```

crear el directori
```
mkdir -p /var/samba/public
```

permisos
```
chmod 777 /var/samba/public
```

configuració de samba
```
vim /etc/samba/smb.conf
```

```
cat /etc/samba/smb.conf
[public]
	comment = Carpeta publica
	path=/var/samba/public
	writable=yes
	guest ok=yes
	create mask=0775
```

Configuració usuaris
```
useradd -m -s /bin/false myuser
smbpasswd -a myuser
```

Reiniciem el sistema
```
service smbd restart
```

accés
```
smbclient //localhost/public
```

```
root@55f6729cf6dc:/# smbclient //localhost/public
Password for [WORKGROUP\root]:
Try "help" to get a list of possible commands.
smb: \> ls
  .                                   D        0  Sun Oct 22 11:33:07 2023
  ..                                  D        0  Sun Oct 22 11:33:07 2023

		33330872 blocks of size 1024. 791156 blocks available
```



### servidor samba
+ Dockerfile
```
FROM debian:latest
RUN apt-get update
RUN apt-get install -y samba smbclient vim

RUN mkdir /opt/docker
COPY * /opt/docker
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
EXPOSE 139 445
```

+ startup.sh
```
#! /bin/bash
# creació de carpetes
mkdir /var/lib/samba/public
chmod 777 /var/lib/samba/public
cp /opt/docker/* /var/lib/samba/public/.

#mkdir /var/lib/samba/privat
#chmod 777 /var/lib/samba/privat

# Configuració de samba
cp /opt/docker/smb.conf /etc/samba/smb.conf

# configuració usuaris
useradd -m -s /bin/bash lila

# Activar els serveis
/usr/sbin/smbd && echo "smb Ok"
/usr/sbin/nmbd -F && echo "nmb Ok"
```

+ smb.conf
```
[public]
	comment = Share de contingut public
	path = /var/lib/samba/public
	public = yes
	browseable = yes
	writable = yes
	printable = no
	guest ok = yes
```


**Generem la imatge**
```
docker build -t sarayj03/repte7_samba
```

**Engegem el container**
```
docker run --rm --name samba -h samba.edt.org -d sarayj03/repte7_samba
```

**Entrem dins del container**
```
docker exec -it samba /bin/bash
smbclient //localhost/public
```

**Entrem com a client al container interactivament**
```
root@samba:/opt/docker# smbclient //172.17.0.2/public
Password for [WORKGROUP\root]:
Anonymous login successful
Try "help" to get a list of possible commands.
smb: \> ls
  .                                   D        0  Sun Oct 22 12:48:37 2023
  ..                                  D        0  Sun Oct 22 12:48:37 2023
  startup.sh                          N      459  Sun Oct 22 12:48:37 2023
  smb.conf                            N      156  Sun Oct 22 12:48:37 2023
  Dockerfile                          N      207  Sun Oct 22 12:48:37 2023
  README.md                           N     1025  Sun Oct 22 12:48:37 2023

		33330872 blocks of size 1024. 835852 blocks available
```
