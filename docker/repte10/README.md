## Rèpliques amb docker-compose
+ no tinguin un nom fixa
+ no poden ser el mateix port


### Fitxers
+ docker-compose.yml


#### Fitxer docker-compose.yml
Tenim diferents servidors que engegem en aquest fitxer, ens fixem que tenim dos servidors web
```
version: "3"
services:
  web:
    image: edtasixm05/getstarted:comptador
    ports:
      - "80"
    networks:
      - webnet
  web2:
    image: sarayj03/web21:base
    ports:
      - "80"
    networks:
      - webnet
  redis:
    image: redis
    ports:
      - "6379:6379"
    command: redis-server --appendonly yes
    networks:
      - webnet
  visualizer:
    image: dockersamples/visualizer:stable
    ports:
      - "8080:8080"
    volumes:
      - "/var/run/docker.sock:/var/run/docker.sock"
    networks:
      - webnet
  portainer:
    image: portainer/portainer
    ports:
      - "9000:9000"
    volumes:
      - "/var/run/docker.sock:/var/run/docker.sock"
    networks:
      - webnet
networks:
  webnet: 
```


### Comprovacions
#### Funcionament d'aquests dos servidors web
+ Fem un docker ps per poder mirar els ports que s'han propagat
```
CONTAINER ID   IMAGE                             COMMAND                  CREATED              STATUS              PORTS                                                           NAMES
a5fb04a0a421   edtasixm05/getstarted:comptador   "python app.py"          About a minute ago   Up About a minute   0.0.0.0:32768->80/tcp, :::32768->80/tcp                         repte10-web-1
93a58ab0bba2   redis                             "docker-entrypoint.s…"   About a minute ago   Up About a minute   0.0.0.0:6379->6379/tcp, :::6379->6379/tcp                       repte10-redis-1
bc65d6260b0c   portainer/portainer               "/portainer"             About a minute ago   Up About a minute   8000/tcp, 9443/tcp, 0.0.0.0:9000->9000/tcp, :::9000->9000/tcp   repte10-portainer-1
897a811e2383   dockersamples/visualizer:stable   "npm start"              About a minute ago   Up About a minute   0.0.0.0:8080->8080/tcp, :::8080->8080/tcp                       repte10-visualizer-1
50e03f71034b   sarayj03/web21:base               "/bin/sh -c 'apachec…"   About a minute ago   Up About a minute   0.0.0.0:32769->80/tcp, :::32769->80/tcp     
```

+ Servidor web 1
```
localhost:32768
```
```
Hello World!
Hostname: a5fb04a0a421
Visits: 1
```


+ Servidor web 2
```
localhost:32769
```
```
Projecte m14
Exemple pÃ gina web 
```


#### --scale web=2
Ens permet desplegar dues rèpliques. 
```
docker compose up -d --scale-web=2
```

+ docker ps
```
CONTAINER ID   IMAGE                             COMMAND                  CREATED         STATUS         PORTS                                                           NAMES
efb8bea07ce3   sarayj03/web21:base               "/bin/sh -c 'apachec…"   9 seconds ago   Up 7 seconds   0.0.0.0:32770->80/tcp, :::32770->80/tcp                         repte10-web2-1
684738736e74   edtasixm05/getstarted:comptador   "python app.py"          9 seconds ago   Up 7 seconds   0.0.0.0:32771->80/tcp, :::32771->80/tcp                         repte10-web-2
b8a4c8ae580a   edtasixm05/getstarted:comptador   "python app.py"          9 seconds ago   Up 7 seconds   0.0.0.0:32772->80/tcp, :::32772->80/tcp                         repte10-web-1
b94a9769ed68   portainer/portainer               "/portainer"             9 seconds ago   Up 7 seconds   8000/tcp, 9443/tcp, 0.0.0.0:9000->9000/tcp, :::9000->9000/tcp   repte10-portainer-1
50c46c2200f1   dockersamples/visualizer:stable   "npm start"              9 seconds ago   Up 8 seconds   0.0.0.0:8080->8080/tcp, :::8080->8080/tcp                       repte10-visualizer-1
ef19eb5fb89d   redis                             "docker-entrypoint.s…"   9 seconds ago   Up 7 seconds   0.0.0.0:6379->6379/tcp, :::6379->6379/tcp                       repte10-redis-1
```

+ servidor web-2
```
localhost:32771
```
```
Hello World!
Hostname: 684738736e74
Visits: 1
```


+ servidor web-1
```
localhost:32772
```
```
Hello World!
Hostname: 684738736e74
Visits: 2
```

