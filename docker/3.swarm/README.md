## Swarm

El model de treball és:
+ docker swarm es pot crear un swarm, afegir i eliminar nodes d'un swarm.
+ docker nodes es pot gestionar l'estat d'un node, pausar-lo, etc.
+ docker stack es poden desplegar aplicaciones en un swarm.

docker service o les configuracions dels serveis dins de docker-compose es poden definir directives de col·locació dels serveis en els nodes.


+ docker swarm
+ docker node


### Docker swarm
Creem el swarm
```
docker swarm init
```

Apagem el swarm
```
docker swarm leave -f
```

### Docker node
Estat del node

Llistar els nodes
```
docker node ls
```

+ pause: s'utilitza per posar un node en pausa
+ drain: s'utilitza per posar un node drenatge, és a dir, permet que les tasques es reprogramin en altres nodes evitant les interrupcions de les aplicacions en execució.
+ active: s'utilitza per posar un node actiu 
```
docker node update --availability pause node1
docker node update --availability drain node1
docker node update --availability active node1
```


#### Colocació de recursos als nodes: label & placement & constraints
Posar etiquetes als nodes
```
docker node update --label-add lloc=local node1
```
```
docker node inspect node1
```


Fem un exemple:
```
docker node update --label-add jordi=i08 i08
```
```
"jordi": "i08",
```


Indicar la colocació als serveis.
Especificar condicions sobre com s'executa un servei
```
  placement:
    constraints: [node.labels.lloc == local ]

  placement:
    constraints: [node.role == manager]
```



