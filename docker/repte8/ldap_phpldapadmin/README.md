## servidor LDAP + phpldapadmin
### Fitxers que necessitem
+ docker-compose.yml
```
version: "2"
services:
  ldap:
    image: sarayj03/ldap23:acl
    container_name: ldap.edt.org
    hostname: ldap.edt.org
    ports:
      - "389:389"
    networks:
      - mynet
  phpldapadmin:
    image: sarayj03/phpldapadmin
    container_name: phpldapadmin.edt.org
    hostname: phpldapadmin.edt.org
    ports:
      - "80:80"
    networks:
      - mynet
networks:
  mynet:
```


### Ordres:
```
docker compose up -d
```
```
docker compose -f docker-compose up -d
```


Per aturar
```
docker compose down
```


### Comprovacions
#### Servidor phpldapadmin
```
localhost/phpldapadmin
```


Podem entrar dins del container
```
docker exec -it ldap.edt.org /bin/bash
```
