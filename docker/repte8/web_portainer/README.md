## continer: web21 i portainer
### Servidor web (ordre)
```
docker run --rm --name web -h web.edt.org -d sarayj03/web21:base
```

### Fitxers que necessitem
+ docker-compose.yml
```
version: "2"
services:
  web:
    image: sarayj03/web21:base
    container_name: web.edt.org
    hostname: web.edt.org
    ports:
      - "80:80"
    networks:
      - mynet
  portainer:
    image: portainer/portainer
    ports:
      - "9000:9000"
    volumes:
      - "/var/run/docker.sock:/var/run/docker.sock"
    networks:
    - mynet
networks:
  mynet:
```


### Ordres:
```
docker compose up -d
```
```
docker compose -f docker-compose up -d
```


Per aturar
```
docker compose down
```
