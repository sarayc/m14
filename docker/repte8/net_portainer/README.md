## servidor net + portainer
### Servidor net
```
docker run --rm --name net -h net.edt.org -d sarayj03/repte7_net
```


### Fitxers que necessitem
+ docker-compose.yml
```
docker run --rm --name web -h web.edt.org -d sarayj03/web21:base
```

### Fitxers que necessitem
+ docker-compose.yml
```
version: "2"
services:
  web:
    image: sarayj03/web21:base
    container_name: web.edt.org
    hostname: web.edt.org
    ports:
      - "80:80"
    networks:
      - mynet
  portainer:
    image: portainer/portainer
    ports:
      - "9000:9000"
    volumes:
      - "/var/run/docker.sock:/var/run/docker.sock"
    networks:
    - mynet
networks:
  mynet:
```


### Ordres:
```
docker compose up -d
```
```
docker compose -f docker-compose up -d
```


Per aturar
```
docker compose down
```


### Comprovacions
#### Servidor net

```
root@net:/opt/docker# telnet localhost 7
Trying 127.0.0.1...
Connected to localhost.
Escape character is '^]'.
echo
echo
```

```
root@net:/opt/docker# telnet 172.27.0.2 7
Trying 172.27.0.2...
Connected to 172.27.0.2.
Escape character is '^]'.
echo
echo
^]
```


### Servidor portainer
```
localhost:9000
```
