## Servidor web + net + portainer
### Servidor web
```
docker run --rm --name web.edt.org -h web.edt.org -d sarayj03/web21:base
```

### Servidor net
```
docker run --rm --name net.edt.org -h net.edt.org -d sarayj03/repte7_net
```


### Fitxers que necessitem
+ docker-compose.yml
```

```


### Ordres:
```
docker compose up -d
```
```
docker compose -f docker-compose up -d
```


Per aturar
```
docker compose down
```


### Comprovacions
#### Servidor net

```
root@net:/opt/docker# telnet localhost 7
Trying 127.0.0.1...
Connected to localhost.
Escape character is '^]'.
echo
echo
```

```
root@net:/opt/docker# telnet 172.27.0.2 7
Trying 172.27.0.2...
Connected to 172.27.0.2.
Escape character is '^]'.
echo
echo
^]
```


### Servidor portainer
```
localhost:9000
```

