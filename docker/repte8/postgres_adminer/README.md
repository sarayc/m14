## servidor net + portainer
### Servidor net
```
docker run --rm --name postgres -d sarayj03/postgres23:base
```


### Fitxers que necessitem
+ docker-compose.yml
```
version: "2"
services:
  postgres:
    image: sarayj03/postgres23
    container_name: postgres
    hostname: postgres.edt.org
    networks:
      - mynet
  portainer:
    image: adminer
    container_name: adminer
    hostname: adminer.edt.org
    ports:
      - "8080:8080"
    networks:
    - mynet
networks:
  mynet:
```


### Ordres:
```
docker compose up -d
```
```
docker compose -f docker-compose up -d
```


Per aturar
```
docker compose down
```


### Comprovacions
#### Servidor postgres
```
docker exec -it postgres /bin/bash
	su -l postgres
	psql training
```

### Servidor adminer
```
localhost:8080
```
```
System: PostgresSQL
Server: db
Username: postgres
Password: passwd
Database: training
```
