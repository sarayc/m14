## Deploy
+ iniciem el swwarm
+ deplegar l'stack i observar les diferents ordres de stack
+ modificar el deploy

#### Iniciem el swarm
En el manager:
```
docker swarm init
```
```
Swarm initialized: current node (atpge2l8qwm2qpve4vvor49h8) is now a manager.

To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-2k1226ugiifuueipsraudt8scfn3p868el1c7b6ti3faa4nzms-6dwykey0y7cgtwzftdxp6n7b1 192.168.1.71:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.
```


#### Depleguem l'stack
Deplega una aplicació com un servei en docker swarm. 
+ -c s'utilitza per especificar l'arxiu de configuració del servei.
```
docker stack deploy -c compose.yml myapp
```

```
Creating network myapp_webnet
Creating service myapp_web
Creating service myapp_redis
Creating service myapp_visualizer
```


Mostra informació sobre les tasques del stack. Las tasques són instàncies individuals dels serveis en execució als nodes.
```
docker stack ps myapp
```
```
ID             NAME                 IMAGE                             NODE      DESIRED STATE   CURRENT STATE                ERROR     PORTS
s1al0bjgapw6   myapp_redis.1        redis:latest                      j03       Running         Running about a minute ago             
tvcz03fjf1oy   myapp_visualizer.1   dockersamples/visualizer:stable   j03       Running         Running 35 seconds ago                 
l08zgvkhczqs   myapp_web.1          edtasixm05/getstarted:comptador   debian    Running         Running about a minute ago             
tlhoog0t2c0m   myapp_web.2          edtasixm05/getstarted:comptador   j03       Running         Running about a minute ago       
```


Mostra informació sobre els serveis.
```
docker stack services myapp
```
```
ID             NAME               MODE         REPLICAS   IMAGE                             PORTS
vtbf2vmqcdd4   myapp_redis        replicated   1/1        redis:latest                      *:6379->6379/tcp
lu2bvq2lcrey   myapp_visualizer   replicated   1/1        dockersamples/visualizer:stable   *:8080->8080/tcp
q60m3qfetuli   myapp_web          replicated   2/2        edtasixm05/getstarted:comptador   *:80->80/tcp
```


#### Modificar el deploy
Quan modifiquem el fitxer compose.yml que descriu quin és l'estat que es vol tenir en el desplegament, no hem d'apagar i tornar a engegar, perquè el deploy és capaç de recalcular i reorganitzar els canvis.
```
vim compose.yml
```
```
docker stack deploy -c docker-compose.yml myapp
```

+ passem de 2 a 5 rèpliques
```
docker stack deploy -c docker-compose.yml myapp
```
```
Updating service myapp_redis (id: vtbf2vmqcdd49f11nmmbyvkvx)
Updating service myapp_visualizer (id: lu2bvq2lcreydo6v3ml9j0vrz)
Updating service myapp_web (id: q60m3qfetulis5yisljdt31o9)
```

**Comprovacions**
Anem al navegador i fem localhost per veure la nostra pàgina. Podem comprovar que canvia el comptador de visites i el hostname cada vegada que refresquem.
```
localhost
```


**comprovacions**
Anem al navegador i busquem el visualitzer, localhost:8080 comprovem que si canviem les rèpliques podem veure les modificacions al visualitzer.
```
localhost:808
```


+ passem de 5 a 2 rèpliques
Aquí comprovem que ens fa una actualització i no fa falta apagar i tornar a engegar.

```
docker stack deploy -c docker-compose.yml  myapp
```
```
Updating service myapp_web (id: q60m3qfetulis5yisljdt31o9)
Updating service myapp_redis (id: vtbf2vmqcdd49f11nmmbyvkvx)
Updating service myapp_visualizer (id: lu2bvq2lcreydo6v3ml9j0vrz)
```

```
docker stack services myapp
```
```
ID             NAME               MODE         REPLICAS   IMAGE                             PORTS
vtbf2vmqcdd4   myapp_redis        replicated   1/1        redis:latest                      *:6379->6379/tcp
lu2bvq2lcrey   myapp_visualizer   replicated   1/1        dockersamples/visualizer:stable   *:8080->8080/tcp
q60m3qfetuli   myapp_web          replicated   2/2        edtasixm05/getstarted:comptador   *:80->80/tcp
```


#### Docker service
Podem gestionar els serveis amb l'ordre docker service tant si s'han desplegat amb docker-compose com amb docker swarm.

```
docker service ls
```
```
vtbf2vmqcdd4   myapp_redis        replicated   1/1        redis:latest                      *:6379->6379/tcp
lu2bvq2lcrey   myapp_visualizer   replicated   1/1        dockersamples/visualizer:stable   *:8080->8080/tcp
q60m3qfetuli   myapp_web          replicated   5/5        edtasixm05/getstarted:comptador   *:80->80/tcp
```


```
docker service ps myapp_web
```
```
l08zgvkhczqs   myapp_web.1   edtasixm05/getstarted:comptador   debian    Running         Running 13 minutes ago             
tlhoog0t2c0m   myapp_web.2   edtasixm05/getstarted:comptador   j03       Running         Running 13 minutes ago             
j4v4u6nhh5va   myapp_web.3   edtasixm05/getstarted:comptador   debian    Running         Running 2 minutes ago              
ip2m24t58xv5   myapp_web.4   edtasixm05/getstarted:comptador   debian    Running         Running 2 minutes ago              
rz4w86b4rjyq   myapp_web.5   edtasixm05/getstarted:comptador   j03       Running         Running 2 minutes ago   
```


```
docker service logs myapp_redis
```
```
myapp_redis.1.s1al0bjgapw6@j03    | 1:C 22 Nov 2023 18:48:45.443 # WARNING Memory overcommit must be enabled! Without it, a background save or replication may fail under low memory condition. Being disabled, it can also cause failures without low memory condition, see https://github.com/jemalloc/jemalloc/issues/1328. To fix this issue add 'vm.overcommit_memory = 1' to /etc/sysctl.conf and then reboot or run the command 'sysctl vm.overcommit_memory=1' for this to take effect.
myapp_redis.1.s1al0bjgapw6@j03    | 1:C 22 Nov 2023 18:48:45.443 * oO0OoO0OoO0Oo Redis is starting oO0OoO0OoO0Oo
myapp_redis.1.s1al0bjgapw6@j03    | 1:C 22 Nov 2023 18:48:45.443 * Redis version=7.2.3, bits=64, commit=00000000, modified=0, pid=1, just started
myapp_redis.1.s1al0bjgapw6@j03    | 1:C 22 Nov 2023 18:48:45.443 * Configuration loaded
myapp_redis.1.s1al0bjgapw6@j03    | 1:M 22 Nov 2023 18:48:45.444 * monotonic clock: POSIX clock_gettime
myapp_redis.1.s1al0bjgapw6@j03    | 1:M 22 Nov 2023 18:48:45.445 * Running mode=standalone, port=6379.
myapp_redis.1.s1al0bjgapw6@j03    | 1:M 22 Nov 2023 18:48:45.446 * Server initialized
myapp_redis.1.s1al0bjgapw6@j03    | 1:M 22 Nov 2023 18:48:45.499 * Creating AOF base file appendonly.aof.1.base.rdb on server start
myapp_redis.1.s1al0bjgapw6@j03    | 1:M 22 Nov 2023 18:48:45.611 * Creating AOF incr file appendonly.aof.1.incr.aof on server start
myapp_redis.1.s1al0bjgapw6@j03    | 1:M 22 Nov 2023 18:48:45.611 * Ready to accept connections tcp
```


```
docker serive rm myapp_redis
```
