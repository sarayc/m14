## comptador de visites
### Fitxers:
+ Dockerfile
+ app.py
+ requirements.txt
+ docker-compose.yml

#### Fitxer Dockerfile
```
FROM python:3.7-alpine
WORKDIR /code
ENV FLASK_APP app.py
ENV FLASK_RUN_HOST 0.0.0.0
RUN apk add --no-cache gcc musl-dev linux-headers
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
COPY . .
CMD ["flask", "run"]
```


#### Fitxer app.py
```
import time
import redis
from flask import Flask

app = Flask(__name__)
cache = redis.Redis(host='redis', port=6379)

def get_hit_count():
    retries = 5
    while True:
        try:
            return cache.incr('hits')
        except redis.exceptions.ConnectionError as exc:
            if retries == 0:
                raise exc
            retries -= 1
            time.sleep(0.5)

@app.route('/')
def hello():
    count = get_hit_count()
    return 'Hello World! I have been seen {} times.\n'.format(count)
```


#### Fitxer requirements.txt
```
Flask
redis
```

#### Fitxer docker-compose.yml
```
version: '3'
services:
  web:
    build: .
    ports:
      - "5000:5000"
    volumes:
      - .:/code
    environment:
      FLASK_ENV: development
  redis:
    image: "redis:alpine"
    ports:
      - "6379:6379"
    volumes:
      - "redis:/data"
volumes:
 redis:
```


### Comprovacions 
#### Primera comprovació (canvis en calent)
+ Al fitxer app.py tenim la funció:
```
@app.route('/')
def hello():
    count = get_hit_count()
    return 'Hello World! I have been seen {} times.\n'.format(count)
```

+ Engegem amb up i editem el fitxer en calent
```
docker compose up -d
```

+ Anem al navegador 
```
Hello World! I have been seen 1 times. 
```

+ Ara si fem vim app.py
```
@app.route('/')
def hello():
    count = get_hit_count()
    return 'Hello World! I have been {} times canvis.\n'.format(count)
```

+ Recarreguem la pàgina i podrem veure els canvis
```
Hello World! I have been seen 2 times canvis.
```


#### Segona comprovació (apagar amb down i veure que quan fem up el comptador no inicialitza desde 0)
+ Modifiquem el fitxer docker-compose.yml, afegim el volum a redis
```
  redis:
    image: "redis:alpine"
    ports:
      - "6379:6379"
    volumes:
      - "redis:/data"
volumes:
 redis:
```

+ Realitzem un docker compose up -d i al navegador ens surt:
```
Hello World! I have been seen 2 times canvis.
```

+ Fem un docker down (ara no ens funcionarà la pàgina)
```
docker compose down
```

+ Tornem a fer un docker compose up -d i al navegador ens surt:
```
Hello World! I have been seen 3 times canvis. 
```


#### Tercera comprovació (/patata)
+ Editem el fitxer app.py
```
@app.route('/patata')
def mypatata():
    count = get_hit_count()
    return 'Funció patata {} times.\n'.format(count)
```

+ Fem un docker compose up -d i al navegador tenim:
```
navegador: localhost:5000/patata
Funció patata 4 times. 
```


#### Quarta comprovació (amb /patata comprovar que podem fer canvis en calent)
+ Editem el fitxer app.py
```
@app.route('/patata')
def mypatata():
    count = get_hit_count()
    return 'Funció patata {} times canvis \n'.format(count)
```

+ refresquem la pàgina 
```
Funció patata 5 times.canvis
```
