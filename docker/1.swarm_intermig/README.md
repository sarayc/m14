## Getting Started with Swarm
Disposarem de tres hosts virtuals.
+ un host serà el manager
+ un host serà el worker1


#### Creem el swarm
En el manager
```
docker swarm init 
```
```
Swarm initialized: current node (jc91vsq4rwhppjy1w8o44w6up) is now a manager.

To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-0a0rwcgajf7aov60rk3lr2iphnxp58m7n3n02elhgniom9b6en-53wdmg0lzvajhtsr2ye1ffezn 192.168.1.71:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.
```


#### Afegim un worker
```
docker swarm join-token worker
```

```
docker node ls
```
```
ID                            HOSTNAME   STATUS    AVAILABILITY   MANAGER STATUS   ENGINE VERSION
shwvzlrzkyon9xz9olmfh3769     debian     Ready     Active                          24.0.0
jc91vsq4rwhppjy1w8o44w6up *   j03        Ready     Active         Leader           23.0.2
```


#### Desplegem un servei
En el manager:
```
docker service create --replicas 1 --name helloworld alpine ping docker.com
```


Mostra una llista dels serveis que s'estan executant 
```
docker service ls
```
```
ID             NAME         MODE         REPLICAS   IMAGE           PORTS
eved25z8wb2n   helloworld   replicated   1/1        alpine:latest   
```


Mostra informació les tasques relacionades abm el servei associat
```
docker service ps helloworld
```
```
ID             NAME           IMAGE           NODE      DESIRED STATE   CURRENT STATE           ERROR     PORTS
m254ririttlq   helloworld.1   alpine:latest   j03       Running         Running 2 minutes ago            
```

**Es deplega una única rèplica**


#### Inspeccionem el servei
```
docker serivce inspect --pretty helloworld
```
```
ID:		eved25z8wb2nc6i4t5i7fmrd7
Name:		helloworld
Service Mode:	Replicated
 Replicas:	1
Placement:
UpdateConfig:
 Parallelism:	1
 On failure:	pause
 Monitoring Period: 5s
 Max failure ratio: 0
 Update order:      stop-first
RollbackConfig:
 Parallelism:	1
 On failure:	pause
 Monitoring Period: 5s
 Max failure ratio: 0
 Rollback order:    stop-first
ContainerSpec:
 Image:		alpine:latest@sha256:eece025e432126ce23f223450a0326fbebde39cdf496a85d8c016293fc851978
 Args:		ping docker.com 
 Init:		false
Resources:
Endpoint Mode:	vip
```


#### Canviem el número de rèpliques
```
docker service scale helloworld=5
```
```
helloworld scaled to 5
overall progress: 5 out of 5 tasks 
1/5: running   [==================================================>] 
2/5: running   [==================================================>] 
3/5: running   [==================================================>] 
4/5: running   [==================================================>] 
5/5: running   [==================================================>] 
verify: Service converged 
```


```
docker service ps helloworld
```
```
m254ririttlq   helloworld.1   alpine:latest   j03       Running         Running 7 minutes ago              
i1excg2cl1bh   helloworld.2   alpine:latest   debian    Running         Running 57 seconds ago             
l3xc4lvm521j   helloworld.3   alpine:latest   j03       Running         Running 58 seconds ago             
90l9pfbp5fg8   helloworld.4   alpine:latest   debian    Running         Running 57 seconds ago             
b4z2er17hes6   helloworld.5   alpine:latest   debian    Running         Running 57 seconds ago    
```


Ara al node tenim:
```
docker ps
```
```
CONTAINER ID   IMAGE           COMMAND             CREATED          STATUS          PORTS     NAMES
2971d67210ce   alpine:latest   "ping docker.com"   39 seconds ago   Up 38 seconds             helloworld.2.i1excg2cl1bhxrymu710ej5g3
12df756c1ecb   alpine:latest   "ping docker.com"   39 seconds ago   Up 38 seconds             helloworld.5.b4z2er17hes60uyirxyqzjse6
d021288c57aa   alpine:latest   "ping docker.com"   39 seconds ago   Up 38 seconds             helloworld.4.90l9pfbp5fg81amv0w801yvsc
```


#### Eliminem el servei
En el manager:
```
docker service rm helloworld
```

Per apagar el swarm fem:
```
docker swarm leave -f
```
