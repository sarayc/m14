# REPTE 1
## @sarayc M14 Curs 2023-2024
### Gitlab sarayc 

Fitxers:
+ Dockerfile

**Generar imatge** 
```
docker build -t sarayj03/repte1:latest .
```
**Executar interactiu**
```
docker run --rm --name repte1 -it sarayj03/repte1:latest /bin/bash
```
**Pujem la imatge al respositori del dockerhub**
```
docker push sarayj03/repte1:latest
```
