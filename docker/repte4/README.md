### ldapserver 2023
#### Fitxers
+ Dockerfile
+ startup.sh
+ edt-org.ldif
+ slapd.conf

#### Fitxer Dockerfile
```
# ldapserver 2023
FROM debian:latest
LABEL author="@sarayc 2023-2024"
LABEL subject="m14 projecte"
RUN apt-get update
RUN apt-get -y install procps iproute2 vim tree nmap ldap-utils slapd less
ARG DEBIAN_FRONTEND=noninteractive

RUN mkdir /opt/docker
COPY * /opt/docker
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
ENTRYPOINT ["/opt/docker/startup.sh"]
EXPOSE 389
```

#### Fitxer startup.sh
```
#! /bin/bash

function initdb(){
	# Esborrar els directoris de configuració i de dades.
	rm -rf /etc/ldap/slapd.d/*
	rm -rf /var/lib/ldap/*

	# Generar el directori de configuració dinàmica slapd.d a partir del fitxer de configuració slapd.conf
	slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d

	# Injectar a baix nivell les dades de la BD 'populate' de l'organització dc=edt,dc=org
	slapadd -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif

	# Assignar la propietat i grup del directori de dades i de configuració a l'usuari openldap.
	chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap

	# Engegar el servei slapd amb el paràmetre que fa debug per mantenir-lo engegat en foreground.
	/usr/sbin/slapd -d0

}

function start(){
	/usr/sbin/slapd -d0 

}

case $1 in 
  initdb)
    echo "initdb"
    initdb;;
  start)
    echo "start"
    start;;
esac
```


#### Ordres

**Generem la imatge**
```
docker build -t sarayj03/ldap_prova .
```

**Engegem el container i provem de manera interactiva**
```
docker run --rm -h ldap.edt.org -it sarayj03/ldap_prova /bin/bash
```

**Persistència de dades amb volums**
```
docker volume create ldap-server
```

**Engegem el container**
```
docker run --rm -h ldap.edt.org -v ldap-server:/etc/ldap/slapd.d -v ldap-server:/var/lib/ldap -d sarayj03/ldap_prova
```

**Entrem al container amb exec i esborrem algún usuari per fer la prova**
```
docker exec -it <nom_container> ldapsearch -x -LLL -H ldap://172.17.0.3 -b 'dc=edt,dc=org' uid=Jordi
```

Esborrem l'usuari Jordi per exemple i ja no estarà, però tornarà a ser perquè el procés de creació de la imatge ldap usada sempre plaxa de nou tota la base de dades. 

Al fitxer startup.sh posem un entrypoint si posem initdb la base de dades funciona com fins ara, però si posem start només s'engegarà el dimoni per tant es mantindrà el que fem.


----------------------------------------------------------------------------------------------------
**Generem la imatge**
```
docker build -t sarayj03/ldap_prova:entrypoint
```

**Creem el volum**
```
docker volume create ldap-server
```

**Engegem el container**
```
docker run --rm -h ldap.edt.org -v ldap-server:/etc/ldap/slapd.d -v ldap-server:/var/lib/ldap -d sarayj03/ldap_prova:entrypoint start
```

**Entrem amb exec i esborrem al Jordi**
Primer el busquem
```
docker exec -it <nom_container> ldapsearch -x -LLL -H ldap://172.17.0.2 -b 'dc=edt,dc=org' uid=Jordi
```

L'esborrem
```
docker exec -it <nom_container> ldapdelete -x -D 'cn=Manager,dc=edt,dc=org' -w secret 'cn=Jordi Mas,ou=usuaris,dc=edt,dc=org'
```

Si ara el busquem, no està:
```
docker exec -it vibrant_archimedes ldapsearch -x -LLL -H ldap://172.17.0.2 -b 'dc=edt,dc=org' uid=Jordi
```

**Si parem el container i el tornem a engegar amb start el Jordi no estarà**
```
docker stop <nom_container>
docker run --rm -h ldap.edt.org -v ldap-server:/etc/ldap/slapd.d -v ldap-server:/var/lib/ldap -d sarayj03/ldap_prova:entrypoint stop
docker exec -it great_taussig ldapsearch -x -LLL -H ldap://172.17.0.2 -b 'dc=edt,dc=org' uid=Jordi
```

----------------------------------------------------------------------------------------------------
#### Engegem amb initdb
**Generem el container i l'engegem amb initdb**
```
docker run --rm -h ldap.edt.org -v ldap-server:/etc/ldap/slapd.d -v ldap-server:/var/lib/ldap -d sarayj03/ldap_prova:entrypoint initdb
```

**Entrem amb exec i esborrem Jordi**
Primer el busquem, ara si que estarà perquè a la funció initdb es restaurà la base de dades
```
docker exec -it beautiful_driscoll ldapsearch -x -LLL -H ldap://172.17.0.2 -b 'dc=edt,dc=org' uid=Jordi
```

L'esborrem
```
docker exec -it beautiful_driscoll ldapdelete -x -D 'cn=Manager,dc=edt,dc=org' -w secret "cn=Jordi Mas,ou=usuaris,dc=edt,dc=org"
```

Ara si el busquem ja no estarà. Aturem el container i tornem a entrar per comprovar que no s'ha guardat la persistència de dades.
```
docker stop <nom_container>
docker run --rm -h ldap.edt.org -v ldap-server:/etc/ldap/slapd.d -v ldap-server:/var/lib/ldap -d sarayj03/ldap_prova:entrypoint initdb
docker exec -it <nom_container> ldapsearch -x -LLL -H ldap://172.17.0.2 -b 'dc=edt,dc=org' uid=Jordi
```
