# REPTE 2
## @sarayc M14 Curs 2023-2024
### Gitlab sarayc 

Fitxers:
+ Dockerfile
+ startup.sh
+ index.html

**Generar imatge** 
```
docker build -t sarayj03/repte2:latest .
```

**Executar**
```
docker run --rm --name repte2 -d sarayj03/repte2
docker run --rm --name repte2 -p 5000:80 -d sarayj03/repte2
```

**Comprovem que el container que es queda en execució**
```
docker ps
```

**Pujem la imatge al respositori del dockerhub**
```
docker push sarayj03/repte2:latest
```

