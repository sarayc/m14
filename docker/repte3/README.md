### servidor postgres
#### Fitxers
+ Dockerfile
+ trainingv7.sql



```
docker run --rm --name postgres -e POSTGRES_PASSWORD=postgres -e POSTGRES_DB=training -d sarayj03/postgres23 
docker run --rm --name postgres -e POSTGRES_PASSWORD=postgres -e POSTGRES_DB=training -d sarayj03/postgres23
docker exec -it postgres psql -d training -U postgres 
```

Dins del container, ens carreguem la taula clients i oficines per fer proves
```
DROP TABLE clients;
DROP TABLE oficines;
```

Si tanquem i tornem a obrir tenim la taula oficines i clients per tant no s'han guardat els canvis.
```
docker stop postgres
docker run --rm --name postgres -e POSTGRES_PASSWORD=postgres -e POSTGRES_DB=training -d sarayj03/postgres23
docker run -it --rm postgres psql -h 172.17.0.2 -U postgres -d training
```

Hem comprovar que tenim les taules que haviem esborrat, per tant:
+ Creem el volum 
```
docker volume create postgres-volume
docker run --rm --name postgres -e POSTGRES_PASSWORD=postgres -e POSTGRES_DB=training -v postgres-volume:/var/lib/postgresql/data -d sarayj03/postgres23
docker ps
```

Ara si entrem i ens carreguem alguna taula de la base de dades o fem una modificació i ja no la tindrem.
```
docker stop postgres
docker run --rm --name postgres -e POSTGRES_PASSWORD=postgres -e POSTGRES_DB=training -v postgres-volume:/var/lib/postgresql/data -d sarayj03/postgres23
docker run --rm -it postgres psql -h 172.17.0.2 -U postgres -d training  
```


-------------------------------------------------------------------------------------------------------------------------
POSTGRES_PASSWORD= variable d'entorn predefinida
