## Comptador de visites (versió 1)
### Fitxers
Necesitarem els fitxers:
+ Dockerfile
+ app.py
+ docker-compose.yml
+ requirements.txt


### Fitxer Dockerfile
Per la creació de la imatge de la app de python.
```
# syntax=docker/dockerfile:1
FROM python:3.7-alpine
WORKDIR /code
ENV FLASK_APP=app.py
ENV FLASK_RUN_HOST=0.0.0.0
RUN apk add --no-cache gcc musl-dev linux-headers
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
EXPOSE 5000
COPY . .
CMD ["flask", "run"]
```


### Fitxer app.py
Aplicació en python que utilitza flask i redis per mostrar una pàgina web amb un comptador de visites.
```
import time
import redis
from flask import Flask

app = Flask(__name__)
cache = redis.Redis(host='redis', port=6379)

def get_hit_count():
    retries = 5
    while True:
        try:
            return cache.incr('hits')
        except redis.exceptions.ConnectionError as exc:
            if retries == 0:
                raise exc
            retries -= 1
            time.sleep(0.5)

@app.route('/')
def hello():
    count = get_hit_count()
    return 'Hello World! I have been seen {} times.\n'.format(count)
```


### Fitxer docker-compose.yml
Per al desplegament de dos serveis, server web (la app) i el servei redis.
```
version: '3'
services:
  web:
    build: .  
    ports:
      - "5000:5000"
  redis:
    image: "redis:alpine"
```


### Fitxer requeriments.txt
Descripció (llibreries) dels requeriments de python per a funcionar. Necessari en fer el pip install.
```
flask
redis
```


### Comprovacions
```
al navegador: localhost:5000
```

------------------------------------------------------------------------------------------------
## Comptador de visites (versió 2)
La versió dos té com a objectiu fer la aplicació 'interactiva'. Per poder fer canvis en calent,
per fer això es modifica el fitxer de docker-compose per fer un bind-mount de manera que es munta
el directori actiu (el de desenvolupament de l'aplicació) al directori /code de dins de l'aplicació.
També es defineix la variable de flask per indicar-li que ha d'actuar en mode desenvolupament.


